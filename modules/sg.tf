resource "aws_security_group" "notejam_server" {
  name        = "${format("sg-%s-notejam-server", var.env)}"
  description = "notejam Server: http"
  vpc_id      = "${data.aws_vpc.app_vpc.id}"
  tags        = "${merge(var.tags, map("Name", format("sg-%s-notejam-server", var.env)))}"
}

resource "aws_security_group_rule" "ingress_grafana_server_grafana_lb" {
  description       = "ingress_grafana_server_grafana_lb"
  type              = "ingress"
  from_port         = 8000
  to_port           = 8000
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.notejam_server.id}"
}
