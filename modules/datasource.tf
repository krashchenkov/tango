data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}

data "aws_vpc" "app_vpc" {
  tags = {
    "Environment" = "${var.env}"
  }
}

data "aws_iam_instance_profile" "notejam_instance_profile" {
  name = "iamip-${var.env}-notejam"
}
